Dissemination of vanA plasmids in Dutch hospitals
================
Sergio Arredondo Alonso

<img src="scripts/report_files/figure-gfm/unnamed-chunk-22-1.png" width="750px" style="display: block; margin: auto;" />

## Code and data availability

Complete code and files used to generate the analysis reported in our
manuscript are fully available and described in the following link
\[<https://gitlab.com/sirarredondo/vancomycin_dissemination/-/blob/master/scripts/report.md>\].

## Issues/Questions

Feel free to drop me an email if you have any questions or suggestions
for improvement <S.ArredondoAlonso@umcutrecht.nl>
